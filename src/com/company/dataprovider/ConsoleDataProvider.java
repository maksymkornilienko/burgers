package com.company.dataprovider;

import com.company.Models.*;
import com.company.enums.AnotherTypes;
import com.company.enums.BurgerSize;
import com.company.enums.FillingTypes;
import com.company.exceptions.AnotherTypeException;
import com.company.exceptions.BurgerSizeException;
import com.company.exceptions.BurgerTypeException;
import com.company.exceptions.FillingTypeException;

import java.util.Scanner;

public class ConsoleDataProvider implements DataProvider {
    private static Scanner scanner = new Scanner(System.in);

    @Override
    public Burger getBurgerType() {
        int burgerType;
        System.out.println("Choose burger: '\n' 1- HAMBURGER '\n' 2- CHESSEBURGER '\n' 3-DOUBLECHESSEBURGER '\n' 4- POTATOHAMBURGER");
        burgerType = scanner.nextInt();
        try {
            switch (burgerType) {
                case 1:
                    return new Hamburger();
                case 2:
                    return new Cheeseburger();
                case 3:
                    return new DoubleСheeseburger();
                case 4:
                    return new PotatoHamburger();
                default:
                    throw new BurgerTypeException();
            }
        } catch (BurgerTypeException e) {
            System.err.println(e.getMessage());
            getBurgerType();
        }
        return new Hamburger();
    }

    @Override
    public BurgerSize getSize() {
        int burgerSize;
        System.out.println("Choose size: '\n' 1- BIG '\n' 2- SMALL'");
        burgerSize = scanner.nextInt();
        try {
            switch (burgerSize) {
                case 1:
                    return BurgerSize.BIG;
                case 2:
                    return BurgerSize.SMALL;
                default:
                    throw new BurgerSizeException();
            }
        } catch (BurgerSizeException e) {
            System.err.println(e.getMessage());
            getSize();
        }
        return BurgerSize.BIG;
    }

    @Override
    public FillingTypes getFillings() {
        int burgerFilling;
        System.out.println("Choose filling: '\n' 1- CHEESE '\n' 2- SALAD'\n' 3- POTATO'\n' 4- NOTHING'");
        burgerFilling = scanner.nextInt();
        try {
            switch (burgerFilling) {
                case 1:
                    return FillingTypes.CHEESE;
                case 2:
                    return FillingTypes.SALAD;
                case 3:
                    return FillingTypes.POTATO;
                    case 4:
                    return FillingTypes.NOTHING;
                default:
                    throw new FillingTypeException();
            }
        } catch (FillingTypeException e) {
            System.err.println(e.getMessage());
            getFillings();
        }
        return FillingTypes.NOTHING;
    }

    @Override
    public AnotherTypes getSpices() {
        String burgerSpices;
        System.out.println("Do you want spices: yes or no");
        burgerSpices = scanner.next();
        try {
            if (burgerSpices.equals("yes")) {
                return AnotherTypes.SPICES;
            } else if (!burgerSpices.equals("no")&& !burgerSpices.equals("yes")){
                throw new AnotherTypeException();
            }
        } catch (AnotherTypeException e) {
            System.err.println(e.getMessage());
            getSpices();
        }
        return AnotherTypes.NO;
    }

    @Override
    public AnotherTypes getMayo() {
        String burgerMayo;
        System.out.println("Do you want spices: yes or no");
        burgerMayo = scanner.next();
        try {
            if (burgerMayo.equals("yes")) {
                return AnotherTypes.MAYO;
            } else if (!burgerMayo.equals("no") && !burgerMayo.equals("yes")){
                throw new AnotherTypeException();
            }
        } catch (AnotherTypeException e) {
            System.err.println(e.getMessage());
            getMayo();
        }
        return AnotherTypes.NO;
    }
}
