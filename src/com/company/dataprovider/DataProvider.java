package com.company.dataprovider;

import com.company.Models.Burger;
import com.company.enums.AnotherTypes;
import com.company.enums.BurgerSize;
import com.company.enums.FillingTypes;

public interface DataProvider {
    Burger getBurgerType();
    BurgerSize getSize();
    FillingTypes getFillings();
    AnotherTypes getSpices();
    AnotherTypes getMayo();
}
