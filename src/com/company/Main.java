package com.company;

import com.company.Models.Burger;
import com.company.dataprovider.ConsoleDataProvider;
import com.company.dataprovider.DataProvider;
import com.company.enums.AnotherTypes;
import com.company.enums.BurgerSize;
import com.company.enums.FillingTypes;

public class Main {

    public static void main(String[] args) {
        DataProvider consoleDataProvider = new ConsoleDataProvider();
        Burger burger = consoleDataProvider.getBurgerType();
        BurgerSize size = consoleDataProvider.getSize();
        FillingTypes filling = consoleDataProvider.getFillings();
        AnotherTypes spice = consoleDataProvider.getSpices();
        AnotherTypes mayo = consoleDataProvider.getMayo();
        burger.setSize(size);
        burger.setTypes(filling);
        burger.setSpices(spice);
        burger.setMayo(mayo);
        System.out.println(burger.toString());
    }
}
