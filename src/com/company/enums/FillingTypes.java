package com.company.enums;

public enum FillingTypes {
    CHEESE(10, 20),
    SALAD(20, 5),
    POTATO(15, 10),
    NOTHING(0, 0);
    private final int tugriks;
    private final int callories;

    FillingTypes(int tugriks, int callories) {
        this.tugriks = tugriks;
        this.callories = callories;
    }

    public int tugriks() {
        return tugriks;
    }

    public int callories() {
        return callories;
    }
}
