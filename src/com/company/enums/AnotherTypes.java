package com.company.enums;

public enum AnotherTypes {
    SPICES(15, 0), MAYO(20, 5), NO(0, 0);
    private final int tugriks;
    private final int callories;

    AnotherTypes(int tugriks, int callories) {
        this.tugriks = tugriks;
        this.callories = callories;
    }

    public int tugriks() {
        return tugriks;
    }

    public int callories() {
        return callories;
    }
}
