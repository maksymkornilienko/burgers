package com.company.enums;

public enum BurgerSize {
    BIG(100, 40),
    SMALL(50, 20);
    private final int tugriks;
    private final int callories;

    BurgerSize(int tugriks, int callories) {
        this.tugriks = tugriks;
        this.callories = callories;
    }

    public int tugriks() {
        return tugriks;
    }

    public int callories() {
        return callories;
    }
}
