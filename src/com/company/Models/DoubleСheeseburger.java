package com.company.Models;

public class DoubleСheeseburger extends Burger {
    @Override
    public String toString() {
        return "DoubleСheeseburger{" +
                "size=" + size +
                ", types=" + types +
                ", spices=" + spices +
                ", mayo=" + mayo +
                ", Total callories :" + this.calculateCallories() +
                ", Total tugriks :" + this.calculateTugriks() +
                '}';
    }
}
