package com.company.Models;

public class Hamburger extends Burger {
    @Override
    public String toString() {
        return "Hamburger{" +
                "size=" + size +
                ", types=" + types +
                ", spices=" + spices +
                ", mayo=" + mayo +
                ", Total callories :" + this.calculateCallories() +
                ", Total tugriks :" + this.calculateTugriks() +
                '}';
    }
}
