package com.company.Models;

import com.company.enums.AnotherTypes;
import com.company.enums.BurgerSize;
import com.company.enums.FillingTypes;

public class PotatoHamburger extends Burger {
    @Override
    public String toString() {
        return "PotatoHamburger{" +
                "size=" + size +
                ", types=" + types +
                ", spices=" + spices +
                ", mayo=" + mayo +
                ", Total callories :" + this.calculateCallories() +
                ", Total tugriks :" + this.calculateTugriks() +
                '}';
    }
}
