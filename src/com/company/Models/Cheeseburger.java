package com.company.Models;

public class Cheeseburger extends Burger {
    @Override
    public String toString() {
        return "Cheeseburger{" +
                "size=" + size.name() +
                ", types=" + types.name() +
                ", spices=" + spices.name() +
                ", mayo=" + mayo.name() +
                ", Total callories :" + this.calculateCallories() +
                ", Total tugriks :" + this.calculateTugriks() +
                '}';
    }
}
