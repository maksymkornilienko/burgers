package com.company.Models;

import com.company.enums.AnotherTypes;
import com.company.enums.BurgerSize;
import com.company.enums.FillingTypes;

public class Burger {
    protected BurgerSize size;
    protected FillingTypes types;
    protected AnotherTypes spices;
    protected AnotherTypes mayo;

    public void setSize(BurgerSize size) {
        this.size = size;
    }

    public void setTypes(FillingTypes types) {
        this.types = types;
    }

    public void setSpices(AnotherTypes spices) {
        this.spices = spices;
    }

    public void setMayo(AnotherTypes mayo) {
        this.mayo = mayo;
    }

    protected int calculateTugriks() {
        return this.mayo.tugriks() + this.spices.tugriks() + this.types.tugriks() + this.size.tugriks();
    }
    protected int calculateCallories() {
         return mayo.callories() + spices.callories() + this.types.callories() + this.size.callories();
    }

    @Override
    public String toString() {
        return "Burger{" +
                "size=" + size.name() +
                ", types=" + types.name() +
                ", spices=" + spices.name() +
                ", mayo=" + mayo.name() +
                ", Total callories :" + this.calculateCallories() +
                ", Total tugriks :" + this.calculateTugriks() +
                '}';
    }
}
