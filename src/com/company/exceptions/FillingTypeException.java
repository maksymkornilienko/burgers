package com.company.exceptions;

public class FillingTypeException extends Exception {
    public FillingTypeException() {
        super("You entered an incorrect filling type");
    }

}
