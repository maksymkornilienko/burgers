package com.company.exceptions;

public class BurgerSizeException extends Exception {
    public BurgerSizeException() {
        super("You entered an incorrect burger size");
    }

}
