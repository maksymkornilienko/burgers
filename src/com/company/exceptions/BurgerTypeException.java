package com.company.exceptions;

public class BurgerTypeException extends Exception {
    public BurgerTypeException() {
        super("You entered an incorrect burger type");
    }

}
