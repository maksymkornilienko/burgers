package com.company.exceptions;

public class AnotherTypeException extends Exception {
    public AnotherTypeException() {
        super("You entered an incorrect value, correct value must have yes or no");
    }

}
